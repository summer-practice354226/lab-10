﻿#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

bool isPrime(int n) {
    if (n <= 1) return false;
    if (n <= 3) return true;
    if (n % 2 == 0 || n % 3 == 0) return false;

    for (int i = 5; i * i <= n; i += 6)
        if (n % i == 0 || n % (i + 2) == 0)
            return false;

    return true;
}

bool isCentralHeptagonal(int n) {
    int h = (7 * n * n - 7 * n + 2) / 2;
    return n == h;
}

int readArrayFromFile(const string& filename) {
    ifstream file(filename);
    if (!file.is_open()) {
        cout << "Failed to open file: " << filename << endl;
        return {};
    }

    int size;
    file >> size;

    int arr(size);
    for (int i = 0; i < size; i++) {
        file >> arr[i];
    }

    file.close();
    return arr;
}

int generateRandomArray(int size) {
    int arr(size);
    for (int i = 0; i < size; i++) {
        arr[i] = rand() % 1000;
    }
    return arr;
}

void printArray(const int& arr) {
    for (int num : arr) {
        cout << num << " ";
    }
    cout << endl;
}

int findCentralHeptagonalPrimes(const int& arr) {
    int result;
    for (int num : arr) {
        if (isPrime(num) && isCentralHeptagonal(num)) {
            result.push_back(num);
        }
    }
    return result;
}

int main() {
    int inputArray = readArrayFromFile("input.txt");

    int centralHeptagonalPrimes = findCentralHeptagonalPrimes(inputArray);

    cout << "Subarray of central heptagonal primes: ";
    printArray(centralHeptagonalPrimes);

    return 0;
}